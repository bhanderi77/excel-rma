export const FRAPPE_API_GET_USER_INFO_ENDPOINT = '/api/resource/User/';
export const CLIENT_CALLBACK_ENDPOINT = '/home/callback';
export const FRAPPE_API_GET_OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token/';
export const FRAPPE_API_GET_CUSTOMER_ENDPOINT = '/api/resource/Customer';
export const FRAPPE_API_GET_ITEM_ENDPOINT = '/api/resource/Item/';
export const TOKEN_ADD_ENDPOINT = '/api/connect/v1/token_added';
export const TOKEN_DELETE_ENDPOINT = '/api/connect/v1/token_delete';
export const SUPPLIER_AFTER_INSERT_ENDPOINT = '/api/supplier/webhook/v1/create';
export const SUPPLIER_ON_UPDATE_ENDPOINT = '/api/supplier/webhook/v1/update';
export const SUPPLIER_ON_TRASH_ENDPOINT = '/api/supplier/webhook/v1/delete';
export const CUSTOMER_AFTER_INSERT_ENDPOINT = '/api/customer/webhook/v1/create';
export const CUSTOMER_ON_UPDATE_ENDPOINT = '/api/customer/webhook/v1/update';
export const CUSTOMER_ON_TRASH_ENDPOINT = '/api/customer/webhook/v1/delete';
export const ITEM_AFTER_INSERT_ENDPOINT = '/api/item/webhook/v1/create';
export const ITEM_ON_UPDATE_ENDPOINT = '/api/item/webhook/v1/update';
export const ITEM_ON_TRASH_ENDPOINT = '/api/item/webhook/v1/delete';
export const FRAPPE_API_SERIAL_NO_ENDPOINT = '/api/resource/Serial%20No';
export const FRAPPE_API_SALES_INVOICE_ENDPOINT =
  '/api/resource/Sales%20Invoice';
export const SERIAL_NO_AFTER_INSERT_ENDPOINT =
  '/api/serial_no/webhook/v1/create';
export const SERIAL_NO_ON_UPDATE_ENDPOINT = '/api/serial_no/webhook/v1/update';
export const OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token';
export const GET_TIME_ZONE_ENDPOINT = '/api/method/frappe.client.get_time_zone';
export const LIST_DELIVERY_NOTE_ENDPOINT = '/api/resource/Delivery%20Note/';
export const LIST_CREDIT_NOTE_ENDPOINT = '/api/resource/Sales%20Invoice';
export const LIST_RETURN_VOUCHER_ENDPOINT = '/api/resource/Payment%20Entry';
export const FRAPPE_API_COMPANY_ENDPOINT = '/api/resource/Company';
export const ERPNEXT_API_WAREHOUSE_ENDPOINT = '/api/resource/Warehouse';
export const POST_DELIVERY_NOTE_ENDPOINT = '/api/resource/Delivery%20Note';
export const API_RESOURCE_TERRITORY = '/api/resource/Territory';
export const FRAPPE_API_GET_GLOBAL_DEFAULTS =
  '/api/resource/Global Defaults/Global Defaults';
export const FRAPPE_API_GET_SYSTEM_SETTINGS =
  '/api/resource/System Settings/System Settings';
export const FRAPPE_API_GET_USER_PERMISSION_ENDPOINT =
  '/api/resource/User Permission';
export const DELIVERY_NOTE_AFTER_INSERT_ENDPOINT =
  '/api/delivery_note/webhook/v1/create';
export const DELIVERY_NOTE_ON_UPDATE_ENDPOINT =
  '/api/delivery_note/webhook/v1/update';
export const DELIVERY_NOTE_ON_TRASH_ENDPOINT =
  '/api/delivery_note/webhook/v1/delete';
export const PURCHASE_INVOICE_ON_SUBMIT_ENDPOINT =
  '/api/purchase_invoice/webhook/v1/create';
export const SALES_INVOICE_ON_SUBMIT_ENDPOINT =
  '/api/sales_invoice/webhook/v1/create';
export const SALES_INVOICE_ON_CANCEL_ENDPOINT =
  '/api/sales_invoice/webhook/v1/cancel';
export const FRAPPE_API_GET_PAYMENT_TERM_TEMPLATE_ENDPOINT =
  '/api/resource/Payment%20Terms%20Template/';
export const FRAPPE_API_PURCHASE_RECEIPT_ENDPOINT =
  '/api/resource/Purchase%20Receipt';
export const ERPNEXT_CUSTOMER_CREDIT_LIMIT_ENDPOINT =
  '/api/resource/Customer%20Credit%20Limit';
export const FRAPPE_API_GET_DOCTYPE_COUNT =
  '/api/method/frappe.client.get_count';
export const FRAPPE_API_INSERT_MANY = '/api/method/frappe.client.insert_many';
export const FRAPPE_CLIENT_CANCEL = '/api/method/frappe.client.cancel';
export const MAP_PO_TO_PI_ENDPOINT =
  '/api/method/erpnext.buying.doctype.purchase_order.purchase_order.make_purchase_invoice';
export const PURCHASE_ORDER_ON_SUBMIT_ENDPOINT =
  '/api/purchase_order/webhook/v1/create';
export const ERPNEXT_PURCHASE_INVOICE_ENDPOINT =
  '/api/resource/Purchase%20Invoice';
export const FRAPPE_CLIENT_SUBMIT_ENDPOINT = '/api/method/frappe.client.submit';
export const API_RESOURCE = '/api/resource/';
export const PURCHASE_INVOICE_ON_CANCEL_ENDPOINT =
  '/api/purchase_invoice/webhook/v1/cancel';
export const STOCK_ENTRY_API_ENDPOINT = '/api/resource/Stock Entry';
