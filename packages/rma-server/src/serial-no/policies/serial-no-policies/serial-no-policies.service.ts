import {
  Injectable,
  BadRequestException,
  NotFoundException,
} from '@nestjs/common';
import { SerialNoService } from '../../entity/serial-no/serial-no.service';
import {
  SerialNoDto,
  ValidateSerialsDto,
  ValidateReturnSerialsDto,
} from '../../entity/serial-no/serial-no-dto';
import { from, of, throwError } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import {
  SERIAL_NO_ALREADY_EXIST,
  ITEM_NOT_FOUND,
  SUPPLIER_NOT_FOUND,
} from '../../../constants/messages';
import { ItemService } from '../../../item/entity/item/item.service';
import { SupplierService } from '../../../supplier/entity/supplier/supplier.service';

@Injectable()
export class SerialNoPoliciesService {
  constructor(
    private readonly serialNoService: SerialNoService,
    private readonly itemService: ItemService,
    private readonly supplierService: SupplierService,
  ) {}

  validateSerial(serialProvider: SerialNoDto) {
    return from(
      this.serialNoService.findOne({ serial_no: serialProvider.serial_no }),
    ).pipe(
      switchMap(serial => {
        if (!serial) {
          return of(true);
        }
        return throwError(new BadRequestException(SERIAL_NO_ALREADY_EXIST));
      }),
    );
  }

  validateItem(serialProvider: SerialNoDto) {
    return from(
      this.itemService.findOne({ item_code: serialProvider.item_code }),
    ).pipe(
      switchMap(item => {
        if (item) {
          return of(true);
        }
        return throwError(new NotFoundException(ITEM_NOT_FOUND));
      }),
    );
  }

  validateSupplier(serialProvider: SerialNoDto) {
    return from(
      this.supplierService.findOne({ name: serialProvider.supplier }),
    ).pipe(
      switchMap(supplier => {
        if (supplier) {
          return of(true);
        }
        return throwError(new NotFoundException(SUPPLIER_NOT_FOUND));
      }),
    );
  }

  validateCompany(serialProvider: SerialNoDto) {
    // validate company
  }

  validateSerials(payload: ValidateSerialsDto) {
    return (payload.validateFor === 'purchase_receipt'
      ? this.validateSerialsForPurchaseReceipt(payload)
      : this.validateSerialsForDeliveryNote(payload)
    ).pipe(
      switchMap(
        (
          data: {
            _id: string;
            notFoundSerials: string[];
            foundSerials: string[];
          }[],
        ) => {
          if (payload.validateFor === 'purchase_receipt') {
            return of({ notFoundSerials: data[0] ? data[0].foundSerials : [] });
          } else {
            return of({
              notFoundSerials: data[0]
                ? data[0].notFoundSerials
                : payload.serials,
            });
          }
        },
      ),
    );
  }

  validateReturnSerials(payload: ValidateReturnSerialsDto) {
    return from(
      this.itemService.findOne({
        $or: [
          { item_code: payload.item_code },
          { item_name: payload.item_code },
        ],
      }),
    ).pipe(
      switchMap(item => {
        return this.serialNoService
          .asyncAggregate([
            {
              $match: {
                $and: [
                  {
                    delivery_note: { $in: payload.delivery_note_names },
                  },
                  {
                    serial_no: { $in: payload.serials },
                  },
                  {
                    item_code: item.item_code,
                  },
                  {
                    warehouse: payload.warehouse,
                  },
                ],
              },
            },
            {
              $group: {
                _id: 'validSerials',
                foundSerials: { $push: '$serial_no' },
              },
            },
            {
              $project: {
                notFoundSerials: {
                  $setDifference: [payload.serials, '$foundSerials'],
                },
              },
            },
          ])
          .pipe(
            switchMap(
              (
                data: {
                  _id: string;
                  notFoundSerials: string[];
                }[],
              ) => {
                return of({
                  notFoundSerials: data[0]
                    ? data[0].notFoundSerials
                    : payload.serials,
                });
              },
            ),
          );
      }),
    );
  }

  validateSerialsForDeliveryNote(payload: ValidateSerialsDto) {
    return from(
      this.itemService.findOne({
        $or: [
          { item_code: payload.item_code },
          { item_name: payload.item_code },
        ],
      }),
    ).pipe(
      switchMap(item => {
        return this.serialNoService.asyncAggregate([
          {
            $match: {
              serial_no: { $in: payload.serials },
              $or: [{ item_code: item.item_code }],
            },
          },
          {
            $group: {
              _id: 'validSerials',
              foundSerials: { $push: '$serial_no' },
            },
          },
          {
            $project: {
              notFoundSerials: {
                $setDifference: [payload.serials, '$foundSerials'],
              },
            },
          },
        ]);
      }),
    );
  }

  validateSerialsForPurchaseReceipt(payload: ValidateSerialsDto) {
    return this.serialNoService.asyncAggregate([
      {
        $match: {
          serial_no: { $in: payload.serials },
          warehouse: { $exists: false },
          $or: [
            { item_code: payload.item_code },
            { item_name: payload.item_code },
          ],
        },
      },
      {
        $group: {
          _id: 'validSerials',
          foundSerials: { $push: '$serial_no' },
        },
      },
    ]);
  }
}
