export const SALES_INVOICE_SUBMITTED = 'Sales Invoice successfully submited';
export const ERROR_FETCHING_SALES_INVOICE =
  'Error fetching Sales Invoice - Code ';
export const SERIAL_ASSIGNED = 'Serials successfully assigned ';
export const ERROR_FETCHING_PURCHASE_INVOICE =
  'Error fetching Purchase Invoice - Code ';
export const INSUFFICIENT_STOCK_BALANCE = 'Insufficient stock';
export const ERROR_FETCHING_WARRANTY_CLAIM =
  'Error fetching Warranty claim - Code ';
